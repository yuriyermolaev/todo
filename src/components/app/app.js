import React, { Component } from 'react';
import './app.css';

import AppHeader from '../app-header';
import SearchPanel from '../search-panel/search-panel';
import TodoList from '../todo-list';
import ItemStatusFilter from '../item-status-filter';
import ItemAddForm from "../item-add-form";

export default class App extends Component {

    maxId = 100;

    state = {
        todoData: [
            this.createTodoItem( 'Drink Coffee' ),
            this.createTodoItem( 'Build React App' ),
            this.createTodoItem( 'Have a lunch' )
        ]
    }

    createTodoItem(label){
        return {
            label,
            important: false,
            done: false,
            id: this.maxId++
        }
    }

    deleteItem = (id) => {
        this.setState( ( {todoData} ) => {

            const   idx = todoData.findIndex(
                        (el)=>el.id === id
                    ),
                    resultNewArray = [
                        ...todoData.slice(0,idx),
                        ...todoData.slice(idx + 1)
                    ];
            return {
                todoData: resultNewArray
            }

        });
    }

    addItem = (text) => {
        const newItem = this.createTodoItem(text);
        this.setState( ({todoData})=>{
            const resultNewArray = [...todoData, newItem ];
            return {
                todoData: resultNewArray
            }
        });
    }

    // toggleProperty = ( arr, id, propName ) => { // also work
    toggleProperty( arr, id, propName ) {
        const   idx = arr.findIndex( (el)=>el.id === id ),
            oldItem = arr[ idx ],
            newItem = {...oldItem, [propName]: !oldItem.[propName] };
        return [
            ...arr.slice(0,idx),
            newItem,
            ...arr.slice(idx + 1)
        ];
    }

    onToggleImportant = (id) => {
        this.setState(({ todoData })=>{
            return {
                todoData: this.toggleProperty( todoData, id, 'important' )
            };
        });
    }

    onToggleDone = (id) => {
        this.setState(({ todoData })=>{
            return {
                todoData: this.toggleProperty( todoData, id, 'done' )
            };
        });
    }

    render() {

        const   { todoData } = this.state,
                doneCount = todoData
                        .filter((el)=>el.done).length,
                todoCount = todoData.length - doneCount;

        return (
            <div className='todo-app'>
                <AppHeader toDo={todoCount} done={doneCount}/>
                <div className='top-panel d-flex'>
                    <SearchPanel />
                    <ItemStatusFilter />
                </div>
                <TodoList
                    todos = { todoData }
                    onDeleted = { this.deleteItem }
                    onToggleImportant = { this.onToggleImportant }
                    onToggleDone = { this.onToggleDone }
                />
                <ItemAddForm
                    onItemAdded={this.addItem}
                />
            </div>
        );
    }


};